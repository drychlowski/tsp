﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsp.Controller;
using tsp.Model;
using tsp.View;

namespace tsp
{
    class Program
    {
        static void Main(string[] args)
        {
            Graph graph = new Graph();
            ConsoleOutput graphView = new ConsoleOutput();
            graphView.SetModel(graph);
            graph.AddObserver(graphView);

            ConsoleInput graphController = new ConsoleInput();
            graphController.SetModel(graph);
            graphController.SetView(graphView);

            graphController.CreateGraph();
            graphController.FindTsp();
           
            Console.ReadKey();
        }
    }
}
