﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tsp.Interfaces
{
    interface IObserver
    {
        void Update();
    }
}
