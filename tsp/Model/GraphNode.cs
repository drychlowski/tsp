﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tsp.Model
{
    class GraphNode
    {
        public string Name { get; }
        public int X { get; }
        public int Y { get; }
        public List<Route> Routes { get; }

        public GraphNode(string name, int x, int y)
        {
            Routes = new List<Route>();
            this.Name = name;
            this.X = x;
            this.Y = y;
        }

        public void AddRoute(GraphNode to)
        {
            Routes.Add(new Route(this, to));
        }
    }
}
