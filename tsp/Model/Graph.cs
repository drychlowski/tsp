﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsp.Interfaces;

namespace tsp.Model
{
    class Graph : ISubject
    {
        //wlasciwosci
        public List<IObserver> Observers { get; } = new List<IObserver>();
        public List<GraphNode> Nodes { get; } = new List<GraphNode>();

        //metody
        public void AddObserver(IObserver o)
        {
            Observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            Observers.Remove(o);
        }

        public void NotifyObservers()
        {
            Observers.ForEach(observer => observer.Update());
        }

        public void AddNode(string name, int x, int y)
        {
            Nodes.Add(new GraphNode(name, x, y));
        }

        public void CreateRoutes()
        {
            foreach (var node in Nodes)
                foreach (var currentNode in Nodes)
                    if (node != currentNode) node.AddRoute(currentNode);
            NotifyObservers();
        }

        public List<Route> Tsp()
        {
            if (Nodes.Count < 2)
                return new List<Route>();

            List<List<Route>> tsp = new List<List<Route>>();
            double minCost;
            //powtorzenie calego algorytmu dla kazdego wezla
            for (int i = 0; i < Nodes.Count(); i++) 
            {
                tsp.Add(new List<Route>());
                GraphNode currentNode = Nodes[i];
                bool[] visited = new bool[Nodes.Count()];
                // petla przechodzaca po kolejnym nieodwiedzonym wezle
                for (int j = 0; j < Nodes.Count(); j++) 
                {
                    Route currentRoute = null;
                    minCost = Double.MaxValue;

                    //jezeli ostatni obieg petli, wroc do wezla poczatkowego
                    if (j == Nodes.Count() - 1) 
                        visited[i] = false;

                    //wybranie drogi o najnizszym koszcie do nieodwiedzonego wezla
                    foreach (var route in currentNode.Routes) 
                        if (!visited[Nodes.IndexOf(route.To)] && route.Cost < minCost)
                        {
                            currentRoute = route;
                            minCost = route.Cost;
                        }

                    visited[Nodes.IndexOf(currentNode)] = true;
                    currentNode = currentRoute.To;
                    tsp[i].Add(currentRoute);
                }
            }

            minCost = Double.MaxValue;
            int minCostIndex = 0;
            double[] routeCost = new double[Nodes.Count()];
            for (int i = 0; i < tsp.Count(); i++)
            {
                foreach (var route in tsp[i])
                    routeCost[i] += route.Cost;

                if (routeCost[i] < minCost)
                {
                    minCost = routeCost[i];
                    minCostIndex = i;
                }
            }

            return tsp[minCostIndex];
        }

        /*
        public void Dijkstra()
        {
            double[] cost = new double[nodes.Count()];
            int[] prev = new int[nodes.Count()];
            bool[] visited = new bool[nodes.Count()];
            int c = 9999, currIndex = 0;

            for (int i = 0; i < nodes.Count(); i++)
            {
                cost[i] = (i == 0) ? 0 : 9999;
                prev[i] = -1;
                visited[i] = false;
            }

            for (int j = 0; j < nodes.Count(); j++)
            {
                for (int i = 0; i < nodes.Count(); i++)
                    if (visited[i] == false & cost[i] < c) currIndex = i;

                GraphNode currentNode = nodes[currIndex];
                visited[currIndex] = true;
                
                foreach (var route in currentNode.Routes)
                {
                    if (visited[nodes.IndexOf(route.To)] == false)
                    {
                        GraphNode neighbor = route.To;
                        int neighborIndex = nodes.IndexOf(neighbor);

                        if (cost[neighborIndex] > cost[currIndex] + route.Cost)
                        {
                            cost[neighborIndex] = cost[currIndex] + route.Cost;
                            prev[neighborIndex] = currIndex;
                        }
                    }
                }
            }

            Console.Write("Index:\t");
            for (int i = 0; i < nodes.Count(); i++)
                Console.Write("{0},\t", i);
            Console.WriteLine();

            Console.Write("Cost:\t");
            for (int i = 0; i < nodes.Count(); i++)
                Console.Write("{0},\t", cost[i]);
            Console.WriteLine();

            Console.Write("From:\t");
            for (int i = 0; i < nodes.Count(); i++)
                Console.Write("{0},\t", prev[i]);
            Console.WriteLine();
           
        }
         */
    }
}
