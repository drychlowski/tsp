﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tsp.Model
{
    class Route
    {
        public GraphNode From { get; }
        public GraphNode To { get; }
        public double Cost { get; }

        public Route(GraphNode a, GraphNode b)
        {
            From = a;
            To = b;
            Cost = Math.Round(CalcCost(), 2);
        }

        public double CalcCost()
        {
            return Math.Sqrt(Math.Pow(To.X - From.X, 2) + Math.Pow(To.Y - From.Y, 2));
        }
    }
}
