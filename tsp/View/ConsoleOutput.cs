﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsp.Model;
using tsp.Interfaces;

namespace tsp.View
{
    class ConsoleOutput : IObserver
    {
        public Graph Model { get; private set; }

        public void SetModel(Graph m)
        {
            Model = m;
        }

        public void Update()
        {
            ShowRoutes();
        }

        public void ShowProgramInfo()
        {
            Console.WriteLine("The Shortest Path, created by Dawid Rychlowski");
        }

        public void ShowRoutes()
        {
            foreach (var node in Model.Nodes)
                foreach (var route in node.Routes)
                    Console.WriteLine("Droga z {0} do {1} ma koszt {2}", route.From.Name, route.To.Name, route.Cost);
        }

        public void ShowRoutes(List<Route> routes)
        {
            foreach (var route in routes)
                Console.WriteLine("Droga z {0} do {1} ma koszt {2}", route.From.Name, route.To.Name, route.Cost);
        }

        public void Write(string x)
        {
            Console.Write(x);
        }
    }
}
