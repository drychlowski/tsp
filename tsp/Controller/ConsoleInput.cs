﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsp.Model;
using tsp.View;

namespace tsp.Controller
{
    class ConsoleInput
    {
        public Graph Model { get; private set; }
        public ConsoleOutput View { get; private set; }

        public void SetModel(Graph m)
        {
            this.Model = m;
        }

        public void SetView(ConsoleOutput v)
        {
            this.View = v;
        }

        public void AddNodes()
        {
            bool exit = false;
            do
            {
                Console.WriteLine("Node name, x, y - for stop type 'stop'");
                string name = Console.ReadLine();
                switch (name)
                {
                    case "stop":
                        exit = true;
                        break;

                    default:
                        bool successfullyParsed = int.TryParse(Console.ReadLine(), out int x) & int.TryParse(Console.ReadLine(), out int y);
                        if (successfullyParsed)
                            Model.AddNode(name, x, y);
                        else
                            View.Write("Wrong x y parameters - try again \n");
                    break;
                }
            }
            while (!exit);
        }

        public void CreateRoutes()
        {
            if (Model.Nodes.Count() < 2)
                throw new InvalidOperationException("Too few nodes, must be >=2");
            else
                Model.CreateRoutes();
        }

        public void CreateGraph()
        {
            AddNodes();
            try
            {
                CreateRoutes();
            }
            catch (InvalidOperationException)
            {
                View.Write("Too few nodes - give at least two \n");
                CreateGraph();
            }
        }

        public void FindTsp()
        {
            double cost = 0;
            foreach (var route in Model.Tsp())
                cost += route.Cost;

            View.Write("TSP: ");
            foreach (var route in Model.Tsp())
            {
                View.Write(route.From.Name);
                View.Write(" - ");
                View.Write(route.To.Name);
                View.Write(" ");
            }

            View.Write("Cost: ");
            View.Write(cost.ToString());
        }


    }
}
